import React from "react";
import PropTypes from "prop-types";

const GetSummary = props => {
  return (
    <form onSubmit={event => props.getSummary(event)}>
      <div className="field">
        <label className="label is-large" htmlFor="input-summary">
          Username
        </label>
        <textarea
          name="long_text"
          id="input-long_text"
          className="input is-large"
          cols="40"
          rows="5"
          placeholder="Enter a Summary"
          required
          value={props.long_text}
          onChange={props.handleChange} // new
        />
      </div>
      <input
        type="submit"
        className="button is-primary is-large is-fullwidth"
        value="Submit"
      />
    </form>
  );
};

GetSummary.propTypes = {
  long_text: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
  getSummary: PropTypes.func.isRequired
};

export default GetSummary;
