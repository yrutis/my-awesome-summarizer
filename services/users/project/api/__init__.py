# services/users/project/api/__init__.py


from flask_restx import Api

from project.api.auth import auth_namespace  # new
from project.api.ping import ping_namespace
from project.api.prediction import prediction_namespace
from project.api.users.view import users_namespace

api = Api(version="1.0", title="Users API", doc="/doc/")

api.add_namespace(ping_namespace, path="/ping")
api.add_namespace(users_namespace, path="/users")
api.add_namespace(prediction_namespace, path="/predict")
api.add_namespace(auth_namespace, path="/auth")  # new
