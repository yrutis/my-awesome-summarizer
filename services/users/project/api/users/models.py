# project/api/models.py

import datetime

import jwt
from flask import current_app
from sqlalchemy.sql import func

from project import bcrypt, db


class User(db.Model):

    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(128), nullable=False)
    email = db.Column(db.String(128), nullable=False)
    password = db.Column(db.String(255), nullable=False)
    active = db.Column(db.Boolean(), default=True, nullable=False)
    created_date = db.Column(db.DateTime, default=func.now(), nullable=False)

    def __init__(self, username, email, password):
        self.username = username
        self.email = email
        self.password = bcrypt.generate_password_hash(password).decode()

    def encode_token(self, user_id, token_type):  # updated
        # new
        if token_type == "access":
            seconds = current_app.config.get("ACCESS_TOKEN_EXPIRATION")
        else:
            seconds = current_app.config.get("REFRESH_TOKEN_EXPIRATION")

        payload = {
            "exp": datetime.datetime.utcnow()
            + datetime.timedelta(seconds=seconds),  # updated
            "iat": datetime.datetime.utcnow(),
            "sub": user_id,
        }
        return jwt.encode(
            payload, current_app.config.get("SECRET_KEY"), algorithm="HS256"
        )

    @staticmethod
    def decode_token(token):
        payload = jwt.decode(token, current_app.config.get("SECRET_KEY"))
        return payload["sub"]


class Summary(db.Model):

    __tablename__ = "summaries"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    long_text = db.Column(
        db.String(), nullable=False
    )  # TODO to be updated max string size
    summary = db.Column(
        db.String(), nullable=False
    )  # TODO to be updated max string size
    created_date = db.Column(db.DateTime, default=func.now(), nullable=False)

    def __init__(self, long_text, summary):
        self.long_text = long_text
        self.summary = summary
