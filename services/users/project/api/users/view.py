# project/api/view.py


from flask import request
from flask_restx import Resource, fields, Namespace  # updated

from project import db
from project.api.users.models import User

# new
from project.api.users.crud import (
    get_user_by_email,
    add_user,
    get_user_by_id,
)


users_namespace = Namespace("users")  # new


# new
user = users_namespace.model(
    "User",
    {
        "id": fields.Integer(readOnly=True),
        "username": fields.String(required=True),
        "email": fields.String(required=True),
        "password": fields.String(required=True),
        "created_date": fields.DateTime,
    },
)


class UsersList(Resource):
    @users_namespace.expect(user, validate=True)  # new
    def post(self):
        post_data = request.get_json()
        username = post_data.get("username")
        email = post_data.get("email")
        password = post_data.get("password")
        response_object = {}

        user = get_user_by_email(email)  # updated
        if user:
            response_object["message"] = "Sorry. That email already exists."
            return response_object, 400
        add_user(username, email, password)
        response_object["message"] = f"{email} was added!"
        return response_object, 201

    @users_namespace.marshal_with(user, as_list=True)
    def get(self):
        return User.query.all(), 200


class Users(Resource):
    @users_namespace.marshal_with(user)
    def get(self, user_id):
        user = get_user_by_id(user_id)  # updated
        if not user:
            users_namespace.abort(404, f"User {user_id} does not exist")
        return user, 200

    @users_namespace.expect(user, validate=True)
    def put(self, user_id):
        post_data = request.get_json()
        username = post_data.get("username")
        email = post_data.get("email")
        response_object = {}

        user = get_user_by_id(user_id)  # updated
        if not user:
            users_namespace.abort(404, f"User {user_id} does not exist")
        user.username = username
        user.email = email
        db.session.commit()
        response_object["message"] = f"{user.id} was updated!"
        return response_object, 200

    def delete(self, user_id):
        response_object = {}
        user = get_user_by_id(user_id)  # updated
        if not user:
            users_namespace.abort(404, f"User {user_id} does not exist")
        db.session.delete(user)
        db.session.commit()
        response_object["message"] = f"{user.email} was removed!"
        return response_object, 200


users_namespace.add_resource(UsersList, "")
users_namespace.add_resource(Users, "/<int:user_id>")
