# project/api/ping.py

from flask import request
from flask_restx import Namespace, Resource, fields

from project import db
from project.api.users.models import Summary
from project.summarizer import keras_model_predict

prediction_namespace = Namespace("predict")  # new


x, actual_headline, prediction_headline = keras_model_predict.predict()


summary = prediction_namespace.model(
    "Summary",
    {
        "id": fields.Integer(readOnly=True),
        "long_text": fields.String(required=True),
        "summary": fields.String,
        "created_date": fields.DateTime,
    },
)


class Prediction(Resource):
    def get(self):
        return {
            "x": x,
            "actual_headline": actual_headline,
            "prediction_headline": prediction_headline,
        }

    # def post(self):
    #     response_object = {}
    #     summary = keras_model_predict.predict_from_input('long_text')
    #     db.session.add(Summary(long_text='long_text', summary=summary))
    #     db.session.commit()
    #     response_object['summary'] = summary
    #     return response_object, 201

    @prediction_namespace.expect(summary, validate=True)  # new
    def post(self):
        post_data = request.get_json()
        long_text = post_data.get("long_text")
        response_object = {}

        summary = keras_model_predict.predict_from_input(long_text)
        db.session.add(Summary(long_text=long_text, summary=summary))
        db.session.commit()
        response_object["long_text"] = long_text
        response_object["summary"] = summary
        return response_object, 201


prediction_namespace.add_resource(Prediction, "")
