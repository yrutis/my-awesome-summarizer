# project/api/ping.py


from flask_restx import Namespace, Resource

ping_namespace = Namespace("ping")  # new


class Ping(Resource):
    def get(self):
        return {"status": "success", "message": "pong222!"}


ping_namespace.add_resource(Ping, "")  # updated
