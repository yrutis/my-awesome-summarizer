from __future__ import print_function

import os

import numpy as np
import pandas as pd

from project.summarizer.seq2seq import Seq2SeqSummarizer


def predict():
    np.random.seed(42)

    current_folder = os.path.dirname(os.path.abspath(__file__))
    print("loading csv file ...")
    data_path = os.path.join(current_folder, "data")
    data_path = os.path.join(data_path, "fake_or_real_news.csv")

    model_dir_path = os.path.join(current_folder, "models")

    df = pd.read_csv(data_path)
    X = df["text"]
    Y = df.title

    config = np.load(
        Seq2SeqSummarizer.get_config_file_path(model_dir_path=model_dir_path),
        allow_pickle=True,
    ).item()

    summarizer = Seq2SeqSummarizer(config)
    summarizer.load_weights(
        weight_file_path=Seq2SeqSummarizer.get_weight_file_path(
            model_dir_path=model_dir_path
        )
    )

    print("start predicting ...")
    x = X[0]
    actual_headline = Y[0]
    headline = summarizer.summarize(x)
    # print('Article: ', x)
    print("Generated Headline: ", headline)
    print("Original Headline: ", actual_headline)

    return x, actual_headline, headline


def predict_from_input(x):
    np.random.seed(42)

    current_folder = os.path.dirname(os.path.abspath(__file__))
    model_dir_path = os.path.join(current_folder, "models")

    config = np.load(
        Seq2SeqSummarizer.get_config_file_path(model_dir_path=model_dir_path),
        allow_pickle=True,
    ).item()

    summarizer = Seq2SeqSummarizer(config)
    summarizer.load_weights(
        weight_file_path=Seq2SeqSummarizer.get_weight_file_path(
            model_dir_path=model_dir_path
        )
    )

    print("start predicting ...")
    headline = summarizer.summarize(x)
    # print('Article: ', x)
    print("Generated Headline: ", headline)

    return headline


if __name__ == "__main__":
    predict()
