# project/tests/test_summary.py


import json


def test_get_summary(test_app):
    client = test_app.test_client()
    resp = client.get("/predict")
    # data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    # TODO Test API


def test_post_summary(test_app, test_database):
    client = test_app.test_client()
    resp = client.post(
        "/predict",
        data=json.dumps({"long_text": "I am a test input"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert isinstance(data["summary"], str)
    assert resp.status_code == 201
    # TODO check if summary returned
