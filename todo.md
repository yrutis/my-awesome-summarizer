# To Do list

## Up Next
### Backend:
- finish routes summary: 
    - insert 5 example summaries in database (seed_db_model cli command)
    - create route get and get single for summaries
- finish tutorial
- finish deploying on aws
- reconfigure to use seperate docker image for summary backend
- pytest monkeypatching
- advanced CI: multistage docker
- add flask admin
- add swagger

### Frontend:
- add linting
- add to gitlab
- how to design the website?
    - create login and signup page
- show some example summaries
- text field to generate new summary


## LATER
- change tensorflow image to tensorflow/serving image
- refactor code
- add all tests
- solve issue to work with entrypoint (dependant on postgres service)


## Done
- setup frontend
- refactor code to put models api calls in different files
- add post route for predict (check tuts)
- adding style like isort
- adding test coverage
- first deployment to heroku
- adding post, get route users
- add post, get route summary
- add gitlab / github
- add continuous delivery (heroku)
- finish routes users
    - update, delete user